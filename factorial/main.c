#include "stdio.h"

int fac_iterative(int n){

	if(n > 12)
		return -2;
	if(n < 0)
		return -1;
	if(!n)
		return 1;
	if(n == 1)
		return 1;

	int result = 1;

	while(n){
		result *= n;
		n -= 1;
	}

	return result;

}//fac_iterative*/

int fac_recursive(int n){

	if(n > 12)
		return -2;
	if(n < 0)
		return -1;
	if(!n)
		return 1;
	if(n == 1)
		return 1;

	return n * fac_recursive(n - 1);

}//fac_recursive*/

int main(){

	printf("factorial (recursive) of 5 is %d\n", fac_recursive(5));
	printf("factorial (iterative) of 5 is %d\n", fac_iterative(5));

	return 0;

}//main*/
