#include <cairo.h>
#include <math.h>

#define SIZE 500
#define LEVEL 3

void draw_triangle(cairo_t *cr);
void draw_smaller_triangle(cairo_t *cr, int level, int x1, int y1, int x2, int y2, int x3, int y3);

int main(int argc, char *argv[]){

	//https://www.cairographics.org/FAQ/
	cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, SIZE, SIZE);
	cairo_t *cr = cairo_create(surface);

	//white background
	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
	cairo_paint(cr);

	draw_triangle(cr);

	cairo_destroy(cr);
	cairo_surface_write_to_png(surface, "sierpinski.png");
	cairo_surface_destroy(surface);

	return 0;

}//main*/

void draw_triangle(cairo_t *cr){

	int x1 = SIZE * 5 / 100;
	int y1 = SIZE - x1;

	cairo_set_source_rgb(cr, 0.9, 0.2, 0.2);

	int x2 = SIZE - x1;
	int y2 = y1;

	int dist = x2 - x1;
	int y3 = y2 - sqrt( (dist * dist) - ( (dist/2) * (dist/2) ) );
	int x3 = SIZE/2;

	cairo_move_to(cr, x1, y1);
	cairo_line_to(cr, x2, y2);
	cairo_line_to(cr, x3, y3);
	cairo_fill(cr);

	draw_smaller_triangle(cr, LEVEL, x1, y1, x2, y2, x3, y3);

}//draw_triangle*/

void draw_smaller_triangle(cairo_t *cr, int level, int x1, int y1, int x2, int y2, int x3, int y3){

	int mid_x1 = (x1 + x2) / 2;
	int mid_x2 = (x3 + x2) / 2;
	int mid_x3 = (x1 + x3) / 2;
	int mid_y1 = (y1 + y2) / 2;
	int mid_y2 = (y3 + y2) / 2;
	int mid_y3 = (y1 + y3) / 2;

	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_move_to(cr, mid_x1, mid_y1);
	cairo_line_to(cr, mid_x2, mid_y2);
	cairo_line_to(cr, mid_x3, mid_y3);
	cairo_fill(cr);

	level -= 1;
	if(level <= 0)
		return;

	draw_smaller_triangle(cr, level, mid_x3, mid_y3, x3, y3, mid_x2, mid_y2);
	draw_smaller_triangle(cr, level, x1, y1, mid_x3, mid_y3, mid_x1, mid_y1);
	draw_smaller_triangle(cr, level, x2, y2, mid_x1, mid_y1, mid_x2, mid_y2);

}//draw_smaller_triangle*/
