#include "stdio.h"

#define SIZE 11
#define TO_FIND 7

int binary_search(int *array, int size, int to_find);
int bsearch_recursive(int *array, int to_find, int start, int end);

int main(){

	int array[SIZE] = {1, 3, 7, 15, 21, 32, 42, 65, 67, 99, 103};

	int index = binary_search(array, SIZE, TO_FIND);
	if(index < 0){
		printf("number not found\n");
	}else{
		printf("number found at %d\n", index);
	}

	return 0;

}//main*/

int binary_search(int *array, int size, int to_find){

	if(!array)
		return -2;
	if(size < 1)
		return -2;
	if(size == 1){
		if(*array == to_find){
			return 0;
		}else{
			return -1;
		}
	}

	return bsearch_recursive(array, to_find, 0, size - 1);

}//binary_search*/

int bsearch_recursive(int *array, int to_find, int start, int end){

	if(end - start < 2){
		if(array[start] == to_find)
			return start;
		if(array[end] == to_find)
			return end;
		return -1;
	}

	int middle = (start + end)/2;

	if(array[middle] == to_find){
		return middle;
	}

	if(array[middle] > to_find){
		bsearch_recursive(array, to_find, start, middle - 1);
	}else{
		bsearch_recursive(array, to_find, middle + 1, end);
	}

}//bsearch_recursive*/
